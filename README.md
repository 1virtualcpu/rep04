This code is based on the behavior of [freelancer-theme](https://github.com/jeromelachaud/freelancer-theme).  
Thanks to the freelancer-theme community for publishing a great site template under the MIT license.
